import logging
import constants
from telegram import Update
from telegram.ext import Updater, CommandHandler, CallbackContext
from isslib import ISSLib
import datetime as dt
import os

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

LOGGER = logging.getLogger(__name__)

# 12 PM daily 5-day forecast of visible passes and update of passes data
def daily_message_callback(context: CallbackContext):
    iss = context.job.context
    bot = context.bot
    
    # Update passes
    iss.update_data()
    passes = iss.get_visual_passes()

    # Create message
    msg = 'Pronóstico para 5 días:\n'
    
    for p in passes:
        msg += str(p)
        msg += '\n'

    if len(passes) == 0:
        msg += "No hay pasos visibles de la ISS en los próximos 5 días\n"
    
    # Send message on the channel
    bot.send_message(chat_id=constants.CH_ID,
                     text=msg)

# Check to alert of imminent pass in the next 10 minutes 
def check_passes_callback(context: CallbackContext):
    iss = context.job.context
    bot = context.bot
    passes = iss.get_visual_passes()
    now = dt.datetime.now(iss.tz)
    ten_mins = dt.timedelta(minutes=10)
    # silent messages before 7 AM
    silent = now.hour < 7

    # Send message for passes in the next 10 minutes
    for p in passes:
        if p.startVisibility - now < ten_mins and not p.notified:
            #Generate photo and msg of the pass trajectory
            pass_photo = iss.genImage(p)
            msg = "Paso visible inminente:\n" + str(p)

            sent = bot.send_photo(chat_id=constants.CH_ID,
                           photo=open(pass_photo, 'rb'),
                           caption=msg,
                           disable_notification=silent)
            if sent:
                p.mark_notified()
            os.remove(pass_photo)

if __name__ == "__main__":
    bot_updater = Updater(token=constants.BOT_API_KEY, use_context=True)

    iss = ISSLib()
    jq = bot_updater.job_queue
    jq.run_repeating(check_passes_callback, interval=120, first=0, context=iss)
    jq.run_daily(daily_message_callback, dt.time(12,0,0, tzinfo=iss.tz), context=iss)

    aps_logger = logging.getLogger('apscheduler')
    aps_logger.setLevel(logging.WARNING)
    
    bot_updater.start_polling()
    bot_updater.idle()