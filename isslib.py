from n2yo import n2yo
from constants import N2YO_APIKEY, LAT, LON, ALT, ISS_ID, TZ_NAME, TMP_FOLDER
import datetime as dt
from skyfield.api import load, EarthSatellite, wgs84
import numpy as np
import matplotlib.pyplot as plt
from pytz import timezone

class ISSLib:

    def __init__(self):
        self.api = n2yo.N2YO(N2YO_APIKEY, LAT, LON, ALT)
        self.ts = load.timescale()
        self.loc = wgs84.latlon(LAT, LON, elevation_m=ALT)
        self.tz = timezone(TZ_NAME)
        self.tle = None
        self.passes = None
        self.last_update = None

    @staticmethod
    def genTimeArray(t0_timestamp, tf_timestamp):
        ts_array = range(t0_timestamp, tf_timestamp)
        dt_array = [dt.datetime.fromtimestamp(x, dt.timezone.utc) for x in ts_array]
        first_min = 0

        for i in range(len(dt_array)):
            if dt_array[i].second == 0:
                first_min = i
                break

        return dt_array, first_min

    def genImage(self, pass_obj):
        tle_lines = self.get_tle().splitlines()
        # Satellite object generation
        iss = EarthSatellite(tle_lines[0], tle_lines[1], 'ISS (ZARYA)', self.ts)

        # Time array of the whole pass in 1 second increments
        dt_array, first_min = self.genTimeArray(pass_obj.rawdata['startVisibility'], pass_obj.rawdata['endUTC'])
        t = self.ts.from_datetimes(dt_array)

        # Arrays of alts and azimuths to plot
        alt, az, _ = (iss - self.loc).at(t).altaz()

        # Plot operations
        fig, ax = plt.subplots(subplot_kw={'projection': 'polar'})
        ax.plot(az.radians, 90 - alt.degrees)
        ax.set_rlim([0,90])
        ax.set_theta_zero_location('N')
        ax.set_rticks([])
        ax.set_xticklabels(['N', 'NE', 'E', 'SE', 'S', 'SO', 'O', 'NO'])
        ax.grid(False)

        # Labels every whole minute in the trajectory
        bbox = dict(boxstyle = 'round', fc = '0.8')

        for i in range(first_min, len(t), 60):
            text = t[i].astimezone(self.tz).strftime('%H:%M')
            ax.plot(az.radians[i], 90 - alt.degrees[i], 'ko')
            ax.annotate(text, (az.radians[i], 90 - alt.degrees[i]), xytext=(10, -5), textcoords ='offset points', bbox=bbox, fontsize=8)

        # Save image to file
        img_name = '{}/image_{}.png'.format(TMP_FOLDER, dt.datetime.now().strftime('%Y%m%d%H%M%S'))
        fig.savefig(img_name, dpi=200, format='png')

        # Return file location
        return img_name

    def get_tle(self):
        if not self.tle:
            self.update_data()

        return self.tle

    def get_visual_passes(self):
        if not self.passes:
            self.update_data()
        return self.passes

    def update_data(self):
        self.tle = self.api.get_tle(ISS_ID)[1]
        raw_passes = self.api.get_visual_passes(ISS_ID, 5, 180)[1]

        # Filter elevation less than 39.5 and magnitude greater than 0
        passes = []
        for p in raw_passes:
            p_obj = Pass(p)
            if p_obj.maxEl >= 39.5 and p_obj.mag <= 0:
                passes.append(p_obj)
        self.passes = passes
        self.last_update = dt.datetime.now()

class Pass:
    def __init__(self, passdata):
        self.tz = timezone(TZ_NAME)
        self.startAz = passdata['startAz']
        self.startAzCompass = passdata['startAzCompass']
        self.startEl = passdata['startEl']
        self.startUTC = dt.datetime.fromtimestamp(passdata['startUTC'], self.tz)
        self.maxAz = passdata['maxAz']
        self.maxAzCompass = passdata['maxAzCompass']
        self.maxEl = passdata['maxEl']
        self.maxUTC = dt.datetime.fromtimestamp(passdata['maxUTC'], self.tz)
        self.endAz = passdata['endAz']
        self.endAzCompass = passdata['endAzCompass']
        self.endEl = passdata['endEl']
        self.endUTC = dt.datetime.fromtimestamp(passdata['endUTC'], self.tz)
        self.mag = passdata['mag']
        self.duration = passdata['duration']
        self.startVisibility = dt.datetime.fromtimestamp(passdata['startVisibility'], self.tz)
        self.notified = False
        self.rawdata = passdata

    def __str__(self):
        return self.summary()

    def summary(self):
        day_and_starttime = self.startVisibility.strftime('%d/%b %H:%M')
        endtime = self.endUTC.strftime('%H:%M')

        return f'{day_and_starttime}-{endtime}: Elev. máx. {self.maxEl}°, Magnitud {self.mag}'

    def mark_notified(self):
        self.notified = True