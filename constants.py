# N2YO REST API Key
N2YO_APIKEY = ''
# NORAD ID number of the ISS
ISS_ID = 25544

# Location coords (Santiago, Chile)
LAT = -33.42628
LON = -70.56656
ALT = 460  # in meters
TZ_NAME = 'America/Santiago'

TMP_FOLDER = './temp'

# Telegram Bot API Key
BOT_API_KEY = ''
# Telegram Channel ID
CH_ID = 
